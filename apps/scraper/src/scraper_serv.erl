-module(scraper_serv).

-behaviour(gen_server).

%% API
-export([start_link/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {name, tref, feed, vals}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link({Name, Prop, Feed}) ->
    gen_server:start_link(Name, ?MODULE, {Prop,Feed}, []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init({SubscriberProp, Feed}) ->
    self() ! first_fetch,
    %% Here we'd fetch the old history we had stored, but given we have
    %% nothing, we'll use a null set of values as a base line
    {ok, #state{name=SubscriberProp, feed=Feed, vals=null_val()}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({timeout, Ref, poll}, S=#state{tref=Ref}) ->
    {noreply, do_fetch(S)};
handle_info(first_fetch, S=#state{}) ->
    {noreply, do_fetch(S)};
handle_info(Info, State) ->
    lager:info("unexpected info: ~p",[Info]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
start_timer(RefreshVal) ->
    erlang:start_timer(delay(RefreshVal), self(), poll).

delay(RefreshVal) ->
    {ok, Min} = application:get_env(scraper, min_delay),
    max(Min, RefreshVal).

null_val() -> {{{0,0,0},{0,0,0}}, "", []}.

do_fetch(S=#state{name=Name, feed=URL, vals=Init}) ->
    case httpc:request(URL) of
        {ok,{{_Vsn, 200, _Msg}, _Headers, Body}} ->
            case get_rss_contents(Body) of
                {ok, TTL, Vals} ->
                    maybe_notify(Name, Init, Vals),
                    S#state{tref = start_timer(TTL), vals = Vals};
                {error, Reason} ->
                    lager:warning("invalid_rss (~p): ~p", [URL, Reason]),
                    S#state{tref = start_timer(0)}
            end;
        {ok,{{_Vsn, Code, _Msg}, _Headers, _Body}} ->
            lager:warning("bad HTTP return code (~p): ~p", [URL, Code]),
            S#state{tref = start_timer(0)};
        {error,Reason} ->
            lager:error("request error (~p): ~p", [URL, Reason]),
            S#state{tref = start_timer(0)}
    end.

get_rss_contents(String) ->
    try
        {ok, {"rss",[{"version","2.0"}], Content}, _Tail} = erlsom:simple_form(String),
        Channel = rss("channel", Content),
        TTL = timer:seconds(list_to_integer(hd(rss("ttl", Channel, "0")))),
        Desc = rss("description", Channel, ""),
        Items = rss_all("item", Channel),
        Date = case rss("pubDate", Channel) of
            undefined -> rss("pubDate", hd(Items));
            Val -> Val
        end,
        {ok, TTL, {to_calendar(Date), Desc, to_proplist(Items)}}
    catch
        Type:Reason -> {error, {Type,Reason,erlang:get_stacktrace()}}
    end.

maybe_notify(Name, {OldDate,_,_}, {NewDate,_,Items}) when NewDate > OldDate ->
    case find_newer(OldDate, Items) of
        [] ->
            lager:debug("no notification required for ~p",[Name]),
            ok;
        NewData ->
            lager:debug("notifying subscribers of ~p", [Name]),
            gproc:send({p,l,Name}, {feed_update, Name, NewData})
    end;
maybe_notify(Name, _Old, _New) ->
    lager:debug("no notification required for ~p",[Name]),
    ok.


%% We assume entries are sorted by >date to make things simpler
find_newer(_, []) -> [];
find_newer(OldDate, [Item|Rest]) ->
    case OldDate < proplists:get_value("pubDate", Item) of
        true -> [Item|find_newer(OldDate, Rest)];
        false -> []
    end.

rss(Key, Data) -> rss(Key, Data, undefined).

rss(Key, Data, Default) ->
    case lists:keyfind(Key, 1, Data) of
        false -> Default;
        {Key, Val} -> Val;
        {Key, _, Val} -> Val
    end.

rss_all(_Key, []) -> [];
rss_all(Key, [{Key,_,Val}|T]) -> [{Key,drop_props(Val)}|rss_all(Key, T)];
rss_all(Key, [_|T]) -> rss_all(Key,T).

%% Annoyed by XML properties we don't care about, we strip them.
drop_props({K,_,V}) -> {K,V};
drop_props([H|T]) -> [drop_props(H)|drop_props(T)];
drop_props(Term) -> Term.

%% We're using DH's date module to parse. Sadly, it doesn't support
%% timezones or offset, so we'll pretend they don't exist because this demo isn't
%% worth hacking in support for that
to_calendar(DateStr) ->
    case dh_date:parse(DateStr) of
        {error, bad_date} ->
            Str = re:replace(DateStr,
                             " (?:[A-Z]{3}|[+-][0-9]+)$",
                             "",
                             [{return, list}]),
            case dh_date:parse(Str) of
                {error, bad_date} -> error({bad_date, DateStr});
                DateTime -> DateTime
            end;
        DateTime ->
            DateTime
    end.

%% Go down RSS elements and turn the pubdate to an Erlang-usable format
to_proplist([]) -> [];
to_proplist([{"item",Item} | Items]) -> [fix_pubdate(Item) | to_proplist(Items)].

fix_pubdate([]) ->
    [];
fix_pubdate([{"pubDate", IoData} | Rest]) ->
    [{"pubDate", to_calendar(unicode:characters_to_list(IoData))} | Rest];
fix_pubdate([H | T]) ->
    [H | fix_pubdate(T)].
