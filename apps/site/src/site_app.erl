-module(site_app).
-behaviour(application).

%% Application callbacks
-export([start/2,
         stop/1]).

start(_StartType, _StartArgs) ->
    %% We don't really need a supervisor. The site app
    %% can be started by anybody anywhere on any port, and
    %% is using a cowboy handler which we do not supervise.
    %% For the sake of getting stuff started with a release,
    %% we boot things with an app.
    Port = case os:getenv("PORT") of
        false -> element(2,application:get_env(site,port));
        N -> list_to_integer(N)
    end,
    Listener = site:start(Port),
    {ok, Pid} = site_sup:start_link(),
    {ok, Pid, Listener}.

stop(Listener) ->
    site:stop(Listener),
    ok.
