{% extends "base.tpl" %}

{% block title %}Prototype Erlang Reader: feeds{% endblock %}

{% block nav %}
<nav>
    <ul>
        <li>Logged in as {{ username }}</li>
        <li><a href="{{ url.base }}signout">Log Out</a></li>
    </ul>
</nav>
{% endblock %}

{% block content %}
{% if error %}
    <p class="error">{{ error }}</p>
{% endif %}

<form action="{{ url.base }}feed" method="post">
    <label for="feed">Add an RSS Feed URL:</label>
    <input type="text" name="url" />
    <input type="submit" value="track feed" />
</form>

<dl>
    {% for feed in feeds %}
        <dt><a class="feedname" href="{{ feed.url }}">{{ feed.url }}</a>:</dt>
        <dd><a href="{{ url.base }}feed/{{ feed.escaped }}">{{ feed.new }} unread entries</a></dd>
    {% endfor %}
</dl>


{% endblock %}
