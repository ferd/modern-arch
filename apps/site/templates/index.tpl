{% extends "base.tpl" %}

{% block title %}Prototype Erlang Reader{% endblock %}

{% block content %}
<h2>Hi there</h2>

<p>This is a prototype for a RSS feed reader. Here are a few of the many
issues with it:</p>

<ul>
    <li>The data model is terrible:
        <ul>
            <li>Everything is non-persistent and lost on every reboot</li>
            <li>URLs and usernames are primary keys,
                and badly normalized to top it off</li>
            <li>Feed data is 100% stored in memory and returned in its
                entirety all the time for a given URL, giving very
                inefficient memory usage</li>
        </ul>
    </li>
    <li>Feed parsing sucks: only RSS is supported, and badly</li>
    <li>No HTTPS support for login/credentials, but hey we use bcrypt!</li>
    <li>No tests except high-level stuff for 'reader' app</li>
    <li>It is done on the run and fairly ugly.</li>
    <li>There's plenty of badly-done abstraction</li>
    <li>It is undocumented, which makes me <a href="http://ferd.ca/don-t-be-a-jerk-write-documentation.html">
        a jerk</a></li>
    <li>banning doesn't cancel out existing sessions, just prevents new ones</li>
    <li>No <a href="http://en.wikipedia.org/wiki/Cross-site_request_forgery">CSRF</a> protection whatsoever</li>
    <li>CSS is inline</li>
    <li>And so much more!</li>
</ul>

<p>Please consider this application as a visual support for my
<a href="http://oreillynet.com/pub/e/2877">presentation with O'Reilly</a>
more than anything else. A proof of concept to show some OTP and
architactural concepts at best, done under a deadline.</p>

<p>That being said, please feel free to either:</p>

<ul>
    <li><a href="{{base.url}}subscribe">subscribe</a></li>
    <li><a href="{{base.url}}auth">log in</a></li>
</ul>

<p>and enjoy your stay.</p>

{% endblock %}

